teamData = [
  {
    css: "wsu",
    teamIcon: "void",
    teamName: "Washington State",
    conference: "7-1",
    overall: "10-1",
    lastGame: "69-28 ariz",
    wOrL: "W",
  },
  {
    css: "uw",
    teamIcon: "void",
    teamName: "Washington",
    conference: "6-2",
    overall: "8-3",
    lastGame: "42-23",
    wOrL: "W",
  },
  {
    css: "su",
    teamIcon: "void",
    teamName: "Stanford",
    conference: "4-3",
    overall: "6-4",
    lastGame: "48-17 ORST",
    wOrL: "W",
  },
  {
    css: "ou",
    teamIcon : "void",
    teamName : "Oregon",
    conference : "4-4",
    overall : "7-4",
    lastGame : "31-29 ASU",
    wOrL : "W",
  },
  {
    css: "cal",
    teamIcon: "void",
    teamName: "California",
    conference: "3-4",
    overall: "6-4",
    lastGame: "15-14 USC",
    wOrL: "W",
  },
  {
    css: "osu",
    teamIcon: "void",
    teamName: "Oregon State",
    conference: "1-7",
    overall: "2-9",
    lastGame: "23-42 WASH",
    wOrL: "L",
  },
]
//
// function addData (currentValue, index, array) {
//   var demoP = document.getElementById("demo");
//
//   var row = demoP.insertRow(0);
//   var cName = row.insertCell(0);
//   var cConference = row.insertCell(-1);
//   var cOverall = row.insertCell(-1);
//   var cLastGame = row.insertCell(-1);
//   var cWOrL = row.insertCell(-1);
//
//   cName.innerHTML = array[index]["teamName"];
//   cConference.innerHTML = array[index]["conference"];
//   cOverall.innerHTML = array[index]["overall"];
//   cWOrL.innerHTML = array[index]["wOrL"];
//   cLastGame.innerHTML = array[index]["lastGame"];
// }
// teamData.forEach(addData);

function myFunction() {
  var selectItem = document.querySelector('li');
  for(i = 0; i < teamData.length; i++) {
    selectItem.innerHTML =  teamData[i]["teamName"];
    selectItem.id = teamData[i]["css"];
    var node = document.createElement("DIV");
    var textnode = document.createTextNode(teamData[i]["conference"]);
    node.appendChild(textnode);
    document.getElementById(teamData[i]["css"]).appendChild(node);

    var node = document.createElement("DIV");
    var textnode = document.createTextNode(teamData[i]["overall"]);
    node.appendChild(textnode);
    document.getElementById(teamData[i]["css"]).appendChild(node);

    selectItem = selectItem.nextSibling.nextSibling;
  }
}
