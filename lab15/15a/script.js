var companyData = [
  {
    name: "Microsoft",
    marketCap: "$381.7 B",
    sales: "$88.6 B",
    profit: "$16.57 B",
    nEmployee: "131,300"
  },
  {
    name: "Symetra Financial",
    marketCap: "$2.7 B",
    sales: "$2.2 B",
    profit: "$205.4 M",
    nEmployee: "1,250"
  },
  {
    name: "Micron Technology",
    marketCap: "$37.6 B",
    sales: "$16.4 B",
    profit: "$5.09 B",
    nEmployee: "34,000"
  },
  {
    name: "F5 Networks",
    marketCap: "$9.5 B",
    sales: "$1.7 B",
    profit: "$420 M",
    nEmployee: "4,460"
  },
  {
    name: "Expedia",
    marketCap: "$10.8 B",
    sales: "$5.8 B",
    profit: "$337 M",
    nEmployee: "22,000"
  },
  {
    name: "Nautilus",
    marketCap: "$476 M",
    sales: "$274.4 M",
    profit: "$26.2 M",
    nEmployee: "480"
  },
  {
    name: "Heritage Financial",
    marketCap: "$531 M",
    sales: "$137.6 M",
    profit: "N/A",
    nEmployee: "N/A"
  },
  {
    name: "Cascade Microtech",
    marketCap: "$239 M",
    sales: "$136 M",
    profit: "$6.1 M",
    nEmployee: "401"
  },
  {
    name: "Nike",
    marketCap: "$83.1 B",
    sales: "$27.8 B",
    profit: "4.24 B",
    nEmployee: "74,400"
  },
  {
    name: "Alaska Air Group",
    marketCap: "$7.9 B",
    sales: "$5.4 B",
    profit: "$1.2 B",
    nEmployee: "21,561 (FTE)"
  },
];


function addData (currentValue, index, array) {
  var demoP = document.getElementById("demo");

  var row = demoP.insertRow(-1);
  var cName = row.insertCell(0);
  var cMarketCap = row.insertCell(-1);
  var cSales = row.insertCell(-1);
  var cProfit = row.insertCell(-1);
  var cEmployees = row.insertCell(-1);

  cName.innerHTML = array[index]["name"];
  cMarketCap.innerHTML = array[index]["marketCap"];
  cSales.innerHTML = array[index]["sales"];
  cProfit.innerHTML = array[index]["profit"];
  cEmployees.innerHTML = array[index]["nEmployee"];

}
companyData.forEach(addData);
