var tempData = [
  {
    day: "sun",
    full_day: "Sunday",
    hi: 51,
    lo: 35,
    weather: "Most Sunny",
    img: "https://ssl.gstatic.com/onebox/weather/48/partly_cloudy.png",
    img_large: "https://ssl.gstatic.com/onebox/weather/64/partly_cloudy.png"
  },
  {
    day: "mon",
    full_day: "Monday",
    hi: 52,
    lo: 36,
    weather: "Sunny",
    img: "https://ssl.gstatic.com/onebox/weather/48/sunny.png",
    img_large: "https://ssl.gstatic.com/onebox/weather/64/sunny.png"
  },
  {
    day: "tues",
    full_day: "Tuesday",
    hi: 53,
    lo: 40,
    weather: "Partly Cloudy",
    img: "https://ssl.gstatic.com/onebox/weather/48/partly_cloudy.png",
    img_large: "https://ssl.gstatic.com/onebox/weather/64/partly_cloudy.png"
  },
  {
    day: "wed",
    full_day: "Wednesday",
    hi: 48,
    lo: 44,
    weather: "Showers",
    img: "https://ssl.gstatic.com/onebox/weather/48/rain_light.png",
    img_large: "https://ssl.gstatic.com/onebox/weather/64/rain_light.png"
  },
  {
    day: "thurs",
    full_day: "Thursday",
    hi: 48,
    lo: 43,
    weather: "Rain",
    img: "https://ssl.gstatic.com/onebox/weather/48/rain.png",
    img_large: "https://ssl.gstatic.com/onebox/weather/64/rain.png"
  }
];

var el =
document.getElementById("largeImg");
el.innerHTML = "<img src='" + tempData[0]["img_large"] + "''>";

var el =
document.getElementById("temp");
el.innerHTML = tempData[0]["hi"] + "&#176;";


function constructArray(array, param) {
  newArray = [];
  for(i = 0; i < array.length; i++) {
    newArray.push(array[i][param]);
  }
  return newArray;
}

function addImages() {
  var imgArray = ["imgOne", "imgTwo",  "imgThree",  "imgFour",  "imgFive"];
  for(i=0; i<imgArray.length; i ++) {
    var el =
    document.getElementById(imgArray[i]);
    el.innerHTML = "<img src='" + tempData[i]['img'] + "'>";
  }
}

function addDays() {
  var idArray = ["dayOne", "dayTwo",  "dayThree",  "dayFour",  "dayFive"];
  for(i=0; i<idArray.length; i ++) {
    var el =
    document.getElementById(idArray[i]);
    el.innerHTML = tempData[i]['day'];
  }
}

function addTempsHi() {
  var tempsArrayHi = ["tempsOneHi", "tempsTwoHi",  "tempsThreeHi",  "tempsFourHi",  "tempsFiveHi"];
  for(i=0; i<tempsArrayHi.length; i ++) {
    var el =
    document.getElementById(tempsArrayHi[i]);
    el.innerHTML = tempData[i]["hi"] + "&#176;";
  }
}

function addTempsLo() {
  var tempsArrayLo = ["tempsOneLo", "tempsTwoLo",  "tempsThreeLo",  "tempsFourLo",  "tempsFiveLo"];
  for(i=0; i<tempsArrayLo.length; i ++) {
    var el =
    document.getElementById(tempsArrayLo[i]);
    el.innerHTML = tempData[i]["lo"] + "&#176;";
  }
}

addDays();
addImages();
addTempsHi();
addTempsLo();

var activeSel = "cellOne"
function updateThis(day) {
  var cellArray = ["cellOne", "cellTwo", "cellThree", "cellFour", "cellFive"];

  var el =
  document.getElementById("dateJS");
  el.innerHTML = tempData[day]["full_day"];

  var el =
  document.getElementById("weatherJS");
  el.innerHTML = tempData[day]["weather"];

  var el =
  document.getElementById("largeImg");
  el.innerHTML = "<img src='" + tempData[day]["img_large"] + "'/>";

  var el =
  document.getElementById("temp");
  el.innerHTML = tempData[day]["hi"] + "&#176;";

  var el =
  document.getElementById(activeSel);
  el.classList.remove("active");


  var el =
  document.getElementById(cellArray[day]);
  el.classList.add("active");

  activeSel = cellArray[day];
  return activeSel;
}


var dataHi = constructArray(tempData, 'hi');
var dataLo = constructArray(tempData, 'lo');

var reducer = (accumulator, currentVal) => accumulator + currentVal;


var el =
document.getElementById("highAverage");
el.innerHTML = dataHi.reduce(reducer) / dataHi.length + "&#176;";

var el =
document.getElementById("lowAverage");
el.innerHTML = dataLo.reduce(reducer) / dataLo.length + "&#176;";
