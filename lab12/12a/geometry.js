function calcCircleGeometries(radius) {
  const pi = Math.PI
  var area = pi * radius^2;
  var circumference = 2 * pi * radius
  var diameter = 2 * radius
  var geometeries = [area, circumference, diameter]

  return geometeries;
}

var greeting = "Hello";
var geo1 = calcCircleGeometries(Math.random());
var geo2 = calcCircleGeometries(Math.random());
var geo3 = calcCircleGeometries(Math.random());

document.write('<p>' + geo1[0] + '<br>' + geo1[1] + '<br>'+ geo1[2] + '<br>'  + '<p>');
document.write('<p>' + geo2[0] + '<br>' + geo2[1] + '<br>'+ geo2[2] + '<br>'  + '<p>');
document.write('<p>' + geo3[0] + '<br>' + geo3[1] + '<br>'+ geo3[2] + '<br>'  + '<p>');
