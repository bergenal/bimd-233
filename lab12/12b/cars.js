// Declares two-dimensional array
var carsArray =
[
['make', 'model', 'year', 'price'],
['porsche', 'cayanne', '2019', '$65,700'],
['acura', 'rdx', '2019', '$38,295'],
['chevrolet', 'silverado', '2019', '$29,795'],
['kia', 'forte', '2019', '$17,690'],
['lexus', 'ux', '2019', '$32,000'],


];
// Capitalize the table - not sure why this was more difficult than it had to be - could not find a camelcase function :(
for (i = 0; i < carsArray.length; i++) {
  if(i == 0) {
    for(j = 0; j < carsArray[i].length; j ++) {
      carsArray[i][j] = carsArray[i][j][0].toUpperCase() + carsArray[i][j].slice(1,carsArray[i][j].length);
      console.log(carsArray[i][j][0].toUpperCase() + carsArray[i][j].slice(1,carsArray[i][j].length));
    }
  }else {
    for(j = 0; j < carsArray[i].length - 2; j ++) {
      carsArray[i][j] = carsArray[i][j][0].toUpperCase() + carsArray[i][j].slice(1,carsArray[i][j].length);
      console.log(carsArray[i][j][0].toUpperCase() + carsArray[i][j].slice(1,carsArray[i][j].length));
    }
  }
}

// Row header
var el =
document.getElementById('make0');
el.textContent = carsArray[0][0];

var el =
document.getElementById('model0');
el.textContent = carsArray[0][1];

var el =
document.getElementById('year0');
el.textContent = carsArray[0][2];

var el =
document.getElementById('price0');
el.textContent = carsArray[0][3];


//Car 1
var el =
document.getElementById('make1');
el.textContent = carsArray[1][0];

var el =
document.getElementById('model1');
el.textContent = carsArray[1][1];

var el =
document.getElementById('year1');
el.textContent = carsArray[1][2];

var el =
document.getElementById('price1');
el.textContent = carsArray[1][3];

// Car2
var el =
document.getElementById('make2');
el.textContent = carsArray[2][0];

var el =
document.getElementById('model2');
el.textContent = carsArray[2][1];

var el =
document.getElementById('year2');
el.textContent = carsArray[2][2];

var el =
document.getElementById('price2');
el.textContent = carsArray[2][3];

// Car3
var el =
document.getElementById('make3');
el.textContent = carsArray[3][0];

var el =
document.getElementById('model3');
el.textContent = carsArray[3][1];

var el =
document.getElementById('year3');
el.textContent = carsArray[3][2];

var el =
document.getElementById('price3');
el.textContent = carsArray[3][3];

// Car4
var el =
document.getElementById('make4');
el.textContent = carsArray[4][0];

var el =
document.getElementById('model4');
el.textContent = carsArray[4][1];

var el =
document.getElementById('year4');
el.textContent = carsArray[4][2];

var el =
document.getElementById('price4');
el.textContent = carsArray[4][3];

// Car5
var el =
document.getElementById('make5');
el.textContent = carsArray[5][0];

var el =
document.getElementById('model5');
el.textContent = carsArray[5][1];

var el =
document.getElementById('year5');
el.textContent = carsArray[5][2];

var el =
document.getElementById('price5');
el.textContent = carsArray[5][3];
