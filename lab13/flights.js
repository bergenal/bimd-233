function createFlight(airline, number, origin, destination, dep_time, arrival_time, arrival_gate) {
  var date = "11/16/2018";
  var flight = [airline, number, origin, destination, dep_time, arrival_time, arrival_gate];
  flight[4] = new Date(date + " " + dep_time);
  flight[5] = new Date(date + " " + arrival_time);
  return flight;
}

function flightDuration(flight) {
  var flightTime = flight[5].getTime() - flight[4].getTime();
  var ms = flightTime;

  var s = ms / 1000;

  var h = parseInt(s / 3600);
  s = s % 3600;

  var m = parseInt(s / 60);
  s = s % 60;
  return(h + ":" + m + ":" + s);
}

var AS2275 = createFlight("Alaska Airlines", "2275", "Boise (BOI)", "Seattle (SEA)", "11:23:00", "12:02:00", "C16");
var AA7557 = createFlight("American Airlines", "7757", "Boise (BOI)", "Seattle (SEA)", "11:23:00", "12:02:00", "A12");
var B663 = createFlight("JetBlue Airways", "663", "New York (JFK)", "Seattle (SEA)", "8:40:00", "12:06:00", "B9");
var AS535 = createFlight("Alaska Airlines", "535", "Ontario (ONT)", "Seattle (SEA)", "9:30:00", "12:11:00", "D4");
var EK3023 = createFlight("Emirates", "3023", "Medford (MFR)", "Seattle (SEA)", "10:45:00", "11:26:00", "A11");

// Flight 1
var el =
document.getElementById("airline1");
el.textContent = AS2275[0];

var el =
document.getElementById("number1");
el.textContent = AS2275[1];

var el =
document.getElementById("origin1");
el.textContent = AS2275[2];

var el =
document.getElementById("arrival1");
el.textContent = AS2275[5];

// Flight 2
var el =
document.getElementById("airline2");
el.textContent = AA7557[0];

var el =
document.getElementById("number2");
el.textContent = AA7557[1];

var el =
document.getElementById("origin2");
el.textContent = AA7557[2];

var el =
document.getElementById("arrival2");
el.textContent = AA7557[5];

// Flight 3
var el =
document.getElementById("airline3");
el.textContent = B663[0];

var el =
document.getElementById("number3");
el.textContent = B663[1];

var el =
document.getElementById("origin3");
el.textContent = B663[2];

var el =
document.getElementById("arrival3");
el.textContent = B663[5];

// Flight 4
var el =
document.getElementById("airline4");
el.textContent = AS535[0];

var el =
document.getElementById("number4");
el.textContent = AS535[1];

var el =
document.getElementById("origin4");
el.textContent = AS535[2];

var el =
document.getElementById("arrival4");
el.textContent = AS535[5];

// Flight 5
var el =
document.getElementById("airline5");
el.textContent = EK3023[0];

var el =
document.getElementById("number5");
el.textContent = EK3023[1];

var el =
document.getElementById("origin5");
el.textContent = EK3023[2];

var el =
document.getElementById("arrival5");
el.textContent = EK3023[5];
