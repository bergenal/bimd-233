$(document).ready(function() {
  $("li").css("id", "uw");
  const states = ["idle", "gather", "process"];
  var state = states[0];
  var words = new Array();
  var ndx = 0;

  $("ul").on("mouseover", "li", function() {
    console.log("x:" + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function() {
    $(this).attr("id", "uw-gold");
  });

  // reset button click
  $("#resetButton").on("click", function(e) {
    var words = [];
    $('.list-item').remove();
  });

  // keypress
  $("input").on("keypress", function(e) {
    var code = e.which;
    newWord = document.getElementById('getValue').value;
    if(code == 13) {
       state = states[1];
    }
    var char = String.fromCharCode(code);
    console.log('key:' + code + '\tstate:' + state);
    switch (state) {
      // idle
      case "idle":
        break;

      // gather
      case "gather":
        words = words + newWord;
        var node = document.createElement("LI");
        var textnode = document.createTextNode(newWord);
        node.appendChild(textnode);
        node.classList.add('list-item');
        document.getElementById('list-container').appendChild(node);
        state = states[2];
        break;

      // process
      case "process":
        document.getElementById('getValue').value = '';
        state = states[0];
        break;

      default:
        break;
    }
  });
});
